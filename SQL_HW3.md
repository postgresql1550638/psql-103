// q1
SELECT country
from country
WHERE country
LIKE 'A%a';

// q2
SELECT * 
FROM country 
WHERE country 
LIKE '_____%n';

// q3
SELECT title 
FROM film 
WHERE title 
ILIKE '%T%T%T%' 
OR 
title
ILIKE 'T%T%T%T'
OR
title
ILIKE 'T%T%T%T%';

// q4
SELECT title, length, rental_rate
FROM film 
WHERE title 
ILIKE 'C%'
AND
length > 90
AND
rental_rate = 2.99;